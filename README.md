# Static Site Contact Form

## Introduction

The Static Site Contact Form module provides a very simple form to be used
with [Tome](https://www.drupal.org/project/tome) or other static generators
to add a  cintact form in a static version of your website.

This **requires** PHP to run on your server, but doesn't require Drupal to
be installed.

## Requirements

- Drupal 8+ to configure and generate static site. This module will try to
  support 8+ for as long as possible, since old sites are the ones most
  probable to go to a static copy.
- **Conflicts with core contact module** because the form is created under
  `/contact`. In any case core contact or any other webform don't work with
   a static copy of your website.
- Server hosting static site must support PHP, just for the form action file
  to send the email.
- You **must** edit the script file with your email recipient address!
- This project depends on **Tome**. There are no code dependencies, but it
  is the only Drupal Static Site generator I know of and the module doesn't
  really make sense to use without Tome. This might change in the future if
  more generators appear.

## Installation

1. Install the module as usual. Configuration is under
 `/admin/config/system/settings/external-form-handler`
2. Upon generating a static version of your website (with Tome Static), copy
   **and configure** the [static_contact_form_action.php.example](form_action_file/static_contact_form_action.php.example)
   in your static html export and transfer it to a server with PHP. You can
   copy it to the root folder, and it will land in the root of your vhost.
3. Rename or change the location of the file as you wish, but remember to
   update configuration and regenerate the static html.
4. **Remember to copy the file again if you re-create the static export**.
   Consider adding this step in your deploy script so you don't forget.
   See following Robo.li task for example:
```php
  /**
   * Deploy static site.
   */
  public function deployStatic() {
    $this->_exec('drush tome:static --uri=https://example.com');
    $this->_exec('cp scripts/form-script.php html/form-script.php');
    $this->_exec('rsync -avz -e ssh html/ user@server.example.com:www/');
  }
``` 

## What about SPAM?

The form, being static in its final state, is visible in the HTML and
you can't use JS to load it. So, a bot without JS can submit the form.

The module has a `honeypot` inspired SPAM check that mitigate that problem.

I also went into great lengths to make sure that the receipient email
address is not made visible anywhere. That is the reason you have to
edit a file by yourself and not from the Drupal UI: because on the
static site everything is plain HTML and visible to bots, so sensitive
date must be in the PHP file.

## Supporting this Module

If you like this module consider buying me a coffee: https://www.buymeacoffee.com/bserem
or hire me for your Drupal website.

## Maintainers

Current maintainers:

- Bill Seremetis (bserem) - https://www.drupal.org/u/bserem

