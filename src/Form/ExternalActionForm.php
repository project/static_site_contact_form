<?php declare(strict_types = 1);

namespace Drupal\static_site_contact_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a contact form which POSTs to an external PHP script.
 * To be used with static site generators.
 */
final class ExternalActionForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'static_site_contact_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#required' => FALSE,
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email address'),
      '#required' => TRUE,
    ];

    // Since captcha might be bypassed without JS, and Drupal will not be there
    // to validate the submission, we add a honeypot inspired element which we
    // hide with CSS and evaluate on the action script.
    $form['h_mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Verify email'),
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#required' => FALSE,
    ];
    $form['send_copy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Receive a copy in your email'),
    ];
    $form['host'] = [
      '#type' => 'hidden',
      '#value' => $this->config('static_site_contact_form.settings')->get('host') ?: \Drupal::request()->getHost(),
    ];
    $form['return_to'] = [
      '#type' => 'hidden',
      '#value' => $this->config('static_site_contact_form.settings')->get('return_to') ?: '/',
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Send'),
      ],
    ];

    $external_url = $this->config('static_site_contact_form.settings')->get('script_location') ?: '/static_contact_form_action.php';

    $form['#attached']['library'] = 'static_site_contact_form/styles';
    $form['#action'] = $external_url;
    $form['#method'] = 'post';

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    // Nothing to add here.
  }
}
