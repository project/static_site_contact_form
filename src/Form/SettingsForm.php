<?php declare(strict_types = 1);

namespace Drupal\static_site_contact_form\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure External Contact Form Handler settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'static_site_contact_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['static_site_contact_form.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['intro'] = [
      '#markup' => $this->t("<code>To:</code> and <code>Cc:</code> values are not configured here, because they would be exposed to the form markup and be made visible to bots.<br>You need to add them to external script file. See the README for more details."),
    ];
    $form['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Which site are the emails sent from'),
      '#required' => FALSE,
      '#description' => $this->t("Type down the path and name of the script. If you don't provide a value <code>" . \Drupal::request()->getHost() . "</code> will be used." ),
      '#default_value' => $this->config('static_site_contact_form.settings')->get('host'),
    ];
    $form['script_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form action script location'),
      '#required' => FALSE,
      '#description' => $this->t("Type down the path and name of the script. If you don't provide a value <code>/static-site-contact-form.php</code> will be used." ),
      '#default_value' => $this->config('static_site_contact_form.settings')->get('script_location'),
    ];
    $form['return_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Where to redirect the user after form is successfully submitted'),
      '#required' => FALSE,
      '#description' => $this->t("Prefix with <code>/</code>. If no address/path is given they will be redirected to the homepage." ),
      '#default_value' => $this->config('static_site_contact_form.settings')->get('return_to'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('static_site_contact_form.settings')
      ->set('host', $form_state->getValue('host'))
      ->set('script_location', $form_state->getValue('script_location'))
      ->set('return_to', $form_state->getValue('return_to'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
